package com.obelov.arbeitskalender;

/**
 * Created by obelov on 16.11.2016.
 */

public class WorkYear implements Comparable<WorkYear>, java.io.Serializable {

    public int m_year;
    public WorkMonth[] m_workMonths;
    public double m_workhours;


    public void WorkYear() // Konstruktor wird nicht aufgerufen???
    {
       // m_workMonths = new WorkMonth[13];
       // for(int i=0; i<13; i++){
       //     m_workMonths[i]=null;
       // }
    }
    public void setYear(int y)
    {
        m_workMonths = new WorkMonth[13];
        for(int i=0; i<13; i++){
            m_workMonths[i]=null;
        }
        m_year = y;
        m_workhours = 0;
    }

    public void addMonth(WorkMonth mon){
        if(m_workMonths[mon.m_month] != null)
        {
            m_workMonths[mon.m_month] = mon;
        }
    }

    @Override
    public int compareTo(WorkYear comp) {

        int compareQuantity = ((WorkYear) comp).m_year;

        //ascending order
        //return this.m_year - compareQuantity;

        //descending order
        return compareQuantity - this.m_year;
    }
}

