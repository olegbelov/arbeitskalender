package com.obelov.arbeitskalender;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Vector;



// TODO
// 1. TextViews anklickbar machen

public class MainActivity extends AppCompatActivity {

    private Vector<WorkYear> workYears = null; // = new Vector<WorkYear>();
    private TableLayout stk;

    Calendar cal = Calendar.getInstance();
    public int thisYear;
    public int thisMonth;

    double default_hour_value = 46;

    private BroadcastReceiver broadcastReceiver;


    @Override
    protected void onResume(){
        super.onResume();
        if(broadcastReceiver == null){
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    TextView tv_coord = (TextView)findViewById(R.id.tv_coordinates);
                    tv_coord.setText("Koordinaten:\n"+intent.getExtras().get("coordinates"));
                    TextView tv_dist = (TextView)findViewById(R.id.tv_dist);
                    tv_dist.setText("Distanz: " +intent.getExtras().get("distance"));
                }
            };
        }
        registerReceiver(broadcastReceiver, new IntentFilter("location_update"));
    }


    /*
    public void PushNotification()
    {
        String tittle="Ihre GPS-Koordinaten";
        String subject="sub";
        String body="body";

        int color = 0x008000;

        NotificationManager notif=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notify=new Notification.Builder
                (getApplicationContext()).setContentTitle(tittle).setContentText(body).
                setContentTitle(subject).setSmallIcon(R.drawable.comm).setColor(color).build();

        notify.flags |= Notification.FLAG_AUTO_CANCEL;
        notif.notify(0, notify);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if(broadcastReceiver !=null){
            unregisterReceiver(broadcastReceiver);
        }
    }
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        stk = (TableLayout) findViewById(R.id.table_main);
        Vector dataVector = new Vector();
        thisYear = cal.get(Calendar.YEAR);
        thisMonth = cal.get(Calendar.MONTH) + 1;



        RelativeLayout relLay = (RelativeLayout) findViewById(R.id.content_main);
        relLay.setOnTouchListener(new OnSwipeTouchListener(this) {

            public void onSwipeRight() {

                if (thisMonth < 12) {
                    thisMonth++;
                    if(!ShowTableOfMonth(thisYear, thisMonth)){
                        thisMonth--;
                        //Toast.makeText(MainActivity.this, "Ende", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            public void onSwipeLeft() {

                if (thisMonth > 1) {
                    thisMonth--;
                    if(!ShowTableOfMonth(thisYear, thisMonth)){
                        thisMonth++;
                        //Toast.makeText(MainActivity.this, "Ende", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            public void onSwipeTop() {
                Toast.makeText(MainActivity.this, "top", Toast.LENGTH_SHORT).show();

            }

            public void onSwipeBottom() {
                Toast.makeText(MainActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }
        });

        setHoursVisible(View.INVISIBLE);
        LoadWorkDaysFromFile();

        if (workYears == null) {
            workYears = new Vector<WorkYear>();

        }
        else {
            ShowTableOfMonth(thisYear, thisMonth);
        }

        final TextView tvSoll = (TextView)findViewById(R.id.tv_sollstunden);

        tvSoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HoursInput();
                int yearIndex= getYearIndex(thisYear);
                if(yearIndex!= -1 && workYears.elementAt(yearIndex).m_workMonths[thisMonth] != null)
                {
                    tvSoll.setText("Sollstunden:   " + Double.toString(workYears.elementAt(yearIndex).m_workMonths[thisMonth].m_hours_to_work));
                }

            }
        });

    }


    void HoursInput() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final EditText txtInput = new EditText(this);
        txtInput.setText("46.0");
        dialogBuilder.setTitle("Sollstunden eingeben");
        dialogBuilder.setMessage("Monat: " + Integer.toString(thisMonth)+"."+Integer.toString(thisYear));
        dialogBuilder.setView(txtInput);
        dialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    String ret1 = "0.0";
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(),
                                txtInput.getText().toString() +  " Sollstunden eingetragen", Toast.LENGTH_SHORT)
                                .show();

                        int yearIndex= getYearIndex(thisYear);
                        if(yearIndex!= -1 && workYears.elementAt(yearIndex).m_workMonths[thisMonth] != null)
                        {
                            TextView tvSoll = (TextView)findViewById(R.id.tv_sollstunden);
                            workYears.elementAt(yearIndex).m_workhours+=workYears.elementAt(yearIndex).m_workMonths[thisMonth].m_hours_to_work;
                            workYears.elementAt(yearIndex).m_workMonths[thisMonth].m_hours_to_work = Double.parseDouble(txtInput.getText().toString());
                            workYears.elementAt(yearIndex).m_workhours-=workYears.elementAt(yearIndex).m_workMonths[thisMonth].m_hours_to_work;

                            tvSoll.setText("Sollstunden:   " + Double.toString(workYears.elementAt(yearIndex).m_workMonths[thisMonth].m_hours_to_work));
                            ShowTableOfMonth(thisYear, thisMonth);

                            SaveDataToFile();
                        }
                    }
                });
                dialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(),
                                "Doch keine Eingabe ...",
                                Toast.LENGTH_SHORT).show();
                    }
                });
        AlertDialog dialogPizzaName = dialogBuilder.create();
        dialogPizzaName.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.clear_all) {
            workYears.clear();
            Toast.makeText(MainActivity.this, "Alle Arbeitstage gelöscht!", Toast.LENGTH_SHORT).show();
            stk.removeAllViews();
            TextView tv = (TextView)findViewById(R.id.tv_monat);
            tv.setText("");
            SaveDataToFile();
            setHoursVisible(View.INVISIBLE);
            return true;
        }

        if (id == R.id.gps_service) {
            startService(new Intent(this, GPS_Track_Service.class));
            return true;
        }
        if (id == R.id.gps_service_stop) {
            stopService(new Intent(this, GPS_Track_Service.class));
            return true;
        }

        if (id == R.id.add_work_location) {

            Intent intent = new Intent(this, MapsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





    public void LoadWorkDaysFromFile() {

        File myWorkDir = getDir("WorkFiles", Context.MODE_PRIVATE);
        File myWorkFile = new File(myWorkDir, "data.woc"); //Getting a file within the dir.

        if(!myWorkFile.exists())
        {
            Toast.makeText(MainActivity.this, "Es existiert keine Datei...", Toast.LENGTH_SHORT).show();
            return;
        }

        AllWorkYears wY = null;
        try {
            FileInputStream fileIn = new FileInputStream(myWorkFile);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            wY = (AllWorkYears) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Workyears class not found");
            c.printStackTrace();
            return;
        }
        workYears = new Vector<WorkYear>(wY.workYears);
        default_hour_value = wY.default_hour_value;
        Collections.sort(workYears);
        //ShowTableOfWorkdays();
    }

    public void SaveDataToFile() {
        File myWorkDir = getDir("WorkFiles", Context.MODE_PRIVATE);
        File myWorkFile = new File(myWorkDir, "data.woc");
        try {
            FileOutputStream fos = new FileOutputStream(myWorkFile);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            // Prepare Data
            AllWorkYears allWy = new AllWorkYears();
            allWy.setAllWorkYear(workYears);
            // Save Data
            os.writeObject(allWy);
            os.close();
            fos.close();
        } catch (Exception ex) {
        }
    }

    public void clickOnNextButon(View v) {


    }

    public void clickOnBeforeButon(View v) {
        SaveDataToFile();
    }



    public TableRow createTopTableRow(Vector<String> textStrngs) {
        TableRow tbRow = new TableRow(this);
        Resources resource = this.getResources();
        tbRow.setBackgroundColor(resource.getColor(R.color.table_top_color));

        for (int i = 0; i < textStrngs.size(); i++) {
            TextView tv = new TextView(this);
            tv.setText(textStrngs.elementAt(i));
            tv.setTextColor(Color.BLACK);
            tv.setGravity(Gravity.CENTER);
            tv.setTextSize(18);
            tv.setPadding(0, 0, 22, 0);
            tbRow.addView(tv);
        }
        return tbRow;
    }

    public TableRow createDayTableRow(Vector<String> textStrngs) {
        TableRow tbRow = new TableRow(this);
        Resources resource = this.getResources();
        tbRow.setBackgroundColor(resource.getColor(R.color.table_color));

        /*
        tbRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView)findViewById(R.id.tv_dist);
                tv.setText("OK!");
            }
        });
        */

        for (int i = 0; i < textStrngs.size(); i++) {
            TextView tv = new TextView(this);
            tv.setText(textStrngs.elementAt(i));
            tv.setTextColor(Color.BLACK);
            tv.setGravity(Gravity.LEFT);
            tv.setTextSize(16);
            tv.setPadding(0, 0, 0, 5);
            tbRow.addView(tv);
        }
        return tbRow;
    }

    public void ShowTableOfWorkdays() {

        stk.removeAllViews();
        Vector<String> topTextStrings = new Vector<String>();
        topTextStrings.add("Datum");
        topTextStrings.add("Beginn");
        topTextStrings.add("Ende");
        topTextStrings.add("Differenz");
        TableRow tbrow0 = createTopTableRow(topTextStrings);
        stk.addView(tbrow0);

        //Testausgabe
        for (int i = 0; i < workYears.size(); i++) {
            for (int j = 12; j > 0; j--) {
                if (workYears.elementAt(i).m_workMonths[j] != null) {
                    TableRow tbrow10 = new TableRow(this);
                    TextView tv111 = new TextView(this);
                    tv111.setText(workYears.elementAt(i).m_workMonths[j].m_monat);
                    tv111.setTextColor(Color.WHITE);
                    tv111.setGravity(Gravity.CENTER);
                    tv111.setTextSize(18);
                    tv111.setLines(1);
                    tv111.setHorizontalFadingEdgeEnabled(true);

                    tbrow10.addView(tv111);
                    stk.addView(tbrow10);

                    for (int k = 31; k > 0; k--)
                        if (workYears.elementAt(i).m_workMonths[j].m_WorkDays[k] != null) {
                            Vector<String> dayStrings = new Vector<String>();
                            dayStrings.add(workYears.elementAt(i).m_workMonths[j].m_WorkDays[k].m_str_datum);
                            dayStrings.add(workYears.elementAt(i).m_workMonths[j].m_WorkDays[k].m_str_zeit_beginn);
                            dayStrings.add(workYears.elementAt(i).m_workMonths[j].m_WorkDays[k].m_str_zeit_ende);
                            dayStrings.add(workYears.elementAt(i).m_workMonths[j].m_WorkDays[k].m_str_zeit_diff);
                            TableRow tbrow = createDayTableRow(dayStrings);
                            stk.addView(tbrow);
                        }
                }
            }
        }
    }

    public void setHoursVisible(int visible)
    {
        TextView tv_sollstunden = (TextView)findViewById(R.id.tv_sollstunden);
        TextView tv_differenz = (TextView)findViewById(R.id.tv_differenz);
        TextView tv_gearbeitet = (TextView)findViewById(R.id.tv_gearbeitet);
        TextView tv_gesamtbilanz = (TextView)findViewById(R.id.tv_gesamtbilanz);
        TextView tv_diffVal = (TextView)findViewById(R.id.tv_diff_value);
        TextView tv_bilVal = (TextView)findViewById(R.id.tv_bilanz_value);

        tv_sollstunden.setVisibility(visible);
        tv_differenz.setVisibility(visible);
        tv_gearbeitet.setVisibility(visible);
        tv_gesamtbilanz.setVisibility(visible);
        tv_diffVal.setVisibility(visible);
        tv_bilVal.setVisibility(visible);

    }


    public int getYearIndex(int year)
    {
        int yearIndex = -1;
        for (int i = 0; i < workYears.size(); i++)
        {
            if (workYears.elementAt(i).m_year == year)
                yearIndex = i;
        }
        return yearIndex;
    }

    public boolean ShowTableOfMonth(int year, int month)
    {
        int yearIndex = getYearIndex(year);
        if (yearIndex == -1) {
            return false;
        }

        if (workYears.elementAt(yearIndex).m_workMonths[month] != null && workYears.elementAt(yearIndex).m_workMonths[month].m_month == month)
        {
            setHoursVisible(View.VISIBLE);
            stk.removeAllViews();

            TextView tv_monat = (TextView) findViewById(R.id.tv_monat);
            tv_monat.setText(workYears.elementAt(yearIndex).m_workMonths[month].m_monat);

            Vector<String> topTextStrings = new Vector<String>();
            topTextStrings.add("Datum      ");
            topTextStrings.add("Beginn    ");
            topTextStrings.add("Ende      ");
            topTextStrings.add("Differenz  ");
            TableRow tbrow0 = createTopTableRow(topTextStrings);
            stk.addView(tbrow0);

            for (int k = 31; k > 0; k--)
            {
                if (workYears.elementAt(yearIndex).m_workMonths[month].m_WorkDays[k] != null)
                {
                    Vector<String> dayStrings = new Vector<String>();
                    dayStrings.add(workYears.elementAt(yearIndex).m_workMonths[month].m_WorkDays[k].m_str_datum);
                    dayStrings.add(workYears.elementAt(yearIndex).m_workMonths[month].m_WorkDays[k].m_str_zeit_beginn);
                    dayStrings.add(workYears.elementAt(yearIndex).m_workMonths[month].m_WorkDays[k].m_str_zeit_ende);
                    dayStrings.add(workYears.elementAt(yearIndex).m_workMonths[month].m_WorkDays[k].m_str_zeit_diff);
                    TableRow tbrow = createDayTableRow(dayStrings);
                    stk.addView(tbrow);
                }
            }
            TextView tvSoll = (TextView)findViewById(R.id.tv_sollstunden);
            TextView tvGearbeitet = (TextView)findViewById(R.id.tv_gearbeitet);
            TextView tvDifferenz = (TextView)findViewById(R.id.tv_differenz);
            TextView tvDifferenzVal = (TextView)findViewById(R.id.tv_diff_value);
            TextView tvBilanz = (TextView)findViewById(R.id.tv_bilanz_value);
            TextView tvBilanzVal = (TextView)findViewById(R.id.tv_bilanz_value);

            tvSoll.setText("Sollstunden:   " + Double.toString(workYears.elementAt(yearIndex).m_workMonths[thisMonth].m_hours_to_work));
            tvGearbeitet.setText("Gearbeitet:      " + Double.toString((workYears.elementAt(yearIndex).m_workMonths[month].m_hours)));

            double diff =  workYears.elementAt(yearIndex).m_workMonths[month].m_hours - workYears.elementAt(yearIndex).m_workMonths[month].m_hours_to_work;
            tvDifferenz.setText("Differenz: ");
            tvDifferenzVal.setText(Double.toString(diff));

            double bilanz = workYears.elementAt(yearIndex).m_workhours;
            tvBilanz.setText("Gesamtbilanz: ");
            tvBilanzVal.setText(Double.toString(bilanz));

            return true;
        }
        else
        {return false;}
    }

    /*
        t1v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView v123 = (TextView) findViewById(R.id.text123);
                v123.setText("dfsfssg");
            }
        });

        t4v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView v123 = (TextView) findViewById(R.id.text123);
                v123.setText("11dfsfssg");

            }
        });
        */

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 0) {

            String dat_beginn = (String) data.getExtras().getSerializable("datum_beginn");
            String dat_ende = (String) data.getExtras().getSerializable("datum_ende");
            String z_beginn = (String) data.getExtras().getSerializable("zeit_beginn");
            String z_ende = (String) data.getExtras().getSerializable("zeit_ende");


            String strToparseBeginn = dat_beginn + " " + z_beginn;
            String strToparseEnde = dat_beginn + " " + z_ende;

            DateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            Date dateBeginn = null;
            Date dateEnde = null;
            try {
                dateBeginn = format.parse(strToparseBeginn);
                dateEnde = format.parse(strToparseEnde);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            WorkDay wd = new WorkDay();
            wd.setBeginn(dateBeginn);
            wd.setEnde(dateEnde);

            String year = dat_beginn.substring(dat_beginn.length()-4);
            String month = dat_beginn.substring(dat_beginn.length()-7, dat_beginn.length()-5);
            String day = dat_beginn.substring(0, 2);

            int year_int = Integer.parseInt(year);
            int month_int = Integer.parseInt(month);
            int day_int = Integer.parseInt(day);


            int yearIndex=-1;
            for(int i=0; i<workYears.size(); i++)
            {
                if(workYears.elementAt(i).m_year == year_int)
                {
                    yearIndex = i;
                }
            }
            if(yearIndex == -1)
            {
                WorkYear wy = new WorkYear();
                wy.setYear(year_int);
                workYears.add(wy);
                yearIndex = workYears.size()-1;
            }
            if( workYears.elementAt(yearIndex).m_workMonths[month_int]==null) {
                WorkMonth wm = new WorkMonth();
                wm.setMonth(month_int);
                workYears.elementAt(yearIndex).m_workMonths[month_int] = wm;
                workYears.elementAt(yearIndex).m_workMonths[month_int].m_hours_to_work = default_hour_value;
                workYears.elementAt(yearIndex).m_workhours-=default_hour_value;
            }
            double workH = ((wd.m_diff_std) + (double)(wd.m_diff_min)/60.0);
            if(workYears.elementAt(yearIndex).m_workMonths[month_int].m_WorkDays[day_int] != null)
            {
                WorkDay wdSub = workYears.elementAt(yearIndex).m_workMonths[month_int].m_WorkDays[day_int];
                double workHSub = ((double)(wdSub.m_diff_std) + (double)(wdSub.m_diff_min)/60.0);
                workYears.elementAt(yearIndex).m_workMonths[month_int].m_hours-=workHSub;
                workYears.elementAt(yearIndex).m_workhours-=workHSub;
            }
            workYears.elementAt(yearIndex).m_workMonths[month_int].m_hours += workH;
            workYears.elementAt(yearIndex).m_workhours+= workH;
            workYears.elementAt(yearIndex).m_workMonths[month_int].m_WorkDays[day_int]=wd;

            TextView tv1 = (TextView)findViewById(R.id.tv_gearbeitet);
            tv1.setText("Gearbeitet:      " + Double.toString((workYears.elementAt(yearIndex).m_workMonths[month_int].m_hours)));

            Collections.sort(workYears);
            Collections.sort(workYears);
            ShowTableOfMonth(year_int, month_int);
            SaveDataToFile();
        }

    }

    public void Click_AddButton(View v) {

        int REQ_CODE_CHILD = 0;
        Intent child = new Intent(this, create_workday.class);
        startActivityForResult(child, REQ_CODE_CHILD);

    }









}
