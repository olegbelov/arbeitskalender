package com.obelov.arbeitskalender;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;
import static java.util.Calendar.getInstance;

public class create_workday extends AppCompatActivity {

    Calendar datum_beginn, datum_ende, zeit_beginn, zeit_ende;

    String str_datum_beginn, str_datum_ende, str_zeit_beginn, str_zeit_ende;
    TextView tv_datum_beginn, tv_datum_ende, tv_zeit_beginn, tv_zeit_ende;
    DateFormat dateFormat;
    DateFormat timeFormat;

    private DatePickerDialog.OnDateSetListener dateBeginnPickerListener
            = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            datum_beginn.set(selectedYear, selectedMonth, selectedDay);
            Date day1 = datum_beginn.getTime();
            str_datum_beginn = dateFormat.format(day1);
            tv_datum_beginn.setText(str_datum_beginn);

            datum_ende.set(selectedYear, selectedMonth, selectedDay);
            str_datum_ende = dateFormat.format(day1);
            tv_datum_ende.setText(str_datum_ende);
        }};

    private DatePickerDialog.OnDateSetListener dateEndPickerListener
            = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            datum_ende.set(selectedYear, selectedMonth, selectedDay);

            Date day1 = datum_ende.getTime();
            str_datum_ende = dateFormat.format(day1);
            tv_datum_ende.setText(str_datum_ende);

            datum_beginn.set(selectedYear, selectedMonth, selectedDay);
            str_datum_beginn = dateFormat.format(day1);
            tv_datum_beginn.setText(str_datum_beginn);

        }};



    private TimePickerDialog.OnTimeSetListener timeBeginnPickerListener
            = new TimePickerDialog.OnTimeSetListener()
    {
        public void onTimeSet(TimePicker view, int selectedHour,
                              int selectedMin)
        {
            zeit_beginn.set(Calendar.HOUR, selectedHour);
            zeit_beginn.set(Calendar.MINUTE, selectedMin);

            Date day1 = zeit_beginn.getTime();
            str_zeit_beginn = timeFormat.format(day1);
            tv_zeit_beginn.setText(str_zeit_beginn);
        }
    };

    private TimePickerDialog.OnTimeSetListener timeEndePickerListener
            = new TimePickerDialog.OnTimeSetListener()
    {
        public void onTimeSet(TimePicker view, int selectedHour,
                              int selectedMin)
        {
            zeit_ende.set(Calendar.HOUR, selectedHour);
            zeit_ende.set(Calendar.MINUTE, selectedMin);

            Date day1 = zeit_ende.getTime();
            str_zeit_ende = timeFormat.format(day1);
            tv_zeit_ende.setText(str_zeit_ende);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_workday);

        datum_beginn = getInstance();
        datum_ende = getInstance();
        zeit_beginn = getInstance();
        zeit_ende = getInstance();

        dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        timeFormat = new SimpleDateFormat("HH:mm");
        Date today = getInstance().getTime();

        String stringDate = dateFormat.format(today);
        String stringTime = timeFormat.format(today);

        str_datum_beginn = stringDate;
        str_datum_ende = stringDate;
        str_zeit_beginn = stringTime;
        str_zeit_ende = stringTime;

        tv_datum_beginn = (TextView) findViewById(R.id.datum_beginn);
        tv_datum_ende = (TextView) findViewById(R.id.datum_ende);

        tv_zeit_beginn = (TextView) findViewById(R.id.zeit_beginn);
        tv_zeit_ende = (TextView) findViewById(R.id.zeit_ende);

        tv_datum_beginn.setText(stringDate);
        tv_datum_ende.setText(stringDate);

        tv_zeit_beginn.setText(stringTime);
        tv_zeit_ende.setText(stringTime);

        tv_datum_beginn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DatePickerDialog datePicker = new
                        DatePickerDialog(v.getContext(), dateBeginnPickerListener, datum_beginn.get(YEAR),
                        datum_beginn.get(MONTH),
                        datum_beginn.get(DAY_OF_MONTH));
                datePicker.show();


            }
        });

        tv_datum_ende.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DatePickerDialog datePicker = new
                        DatePickerDialog(v.getContext(), dateEndPickerListener, datum_ende.get(YEAR),
                        datum_ende.get(MONTH),
                        datum_ende.get(DAY_OF_MONTH));
                datePicker.show();

            }
        });


        tv_zeit_beginn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                TimePickerDialog mTimePickerBeginn = new
                        TimePickerDialog(v.getContext(), timeBeginnPickerListener,
                        zeit_beginn.get(Calendar.HOUR),
                        zeit_beginn.get(Calendar.MINUTE), true);
                mTimePickerBeginn.setTitle("Beginn ");
                mTimePickerBeginn.show();
            }
        });

        tv_zeit_ende.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TimePickerDialog mTimePickerBeginn = new
                        TimePickerDialog(v.getContext(), timeEndePickerListener,
                        zeit_ende.get(Calendar.HOUR),
                        zeit_ende.get(Calendar.MINUTE), true);
                mTimePickerBeginn.setTitle("Ende ");
                mTimePickerBeginn.show();
            }
        });
    }



    public void click_Abbrechen(View v){

        int resultCode = 1;
        Intent resultIntent = new Intent();
        setResult(resultCode, resultIntent);
        finish();
    }

    public void click_Okay(View v){

        int resultCode = 0;
        Intent resultIntent = new Intent();
        resultIntent.putExtra("datum_beginn", str_datum_beginn);
        resultIntent.putExtra("datum_ende", str_datum_ende);
        resultIntent.putExtra("zeit_beginn", str_zeit_beginn);
        resultIntent.putExtra("zeit_ende", str_zeit_ende);
        setResult(resultCode, resultIntent);
        finish();

    }

}
