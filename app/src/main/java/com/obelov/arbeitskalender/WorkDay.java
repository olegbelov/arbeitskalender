package com.obelov.arbeitskalender;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by obelov on 13.11.2016.
 */


public class WorkDay implements java.io.Serializable{


    public String m_str_datum, m_str_zeit_beginn, m_str_zeit_ende, m_str_zeit_diff;
    public Date m_datum, m_time_beginn, m_time_ende, m_time_diff;

    public int m_diff_std, m_diff_min;

    DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
    DateFormat timeFormat = new SimpleDateFormat("HH:mm");

    public void WorkDay()
    {

    }

    public void WorkDay(Date begin, Date end)
    {
        m_time_beginn = (Date) begin.clone();
        m_time_ende = (Date)end.clone();
        m_datum = begin;

        m_str_datum = dateFormat.format(m_time_beginn);
        m_str_zeit_beginn = timeFormat.format(m_time_beginn);
        m_str_zeit_ende = timeFormat.format(m_time_ende);

        m_str_zeit_diff= timeFormat.format(m_time_ende);
        m_time_diff = m_time_ende;

    }

    public void setDate(Date d)
    {
        m_datum = d;
        m_str_datum = m_str_zeit_beginn = dateFormat.format(d);
    }

    public void setBeginn(Date d)
    {
        m_time_beginn = d;
        m_str_datum = dateFormat.format(m_time_beginn);
        m_str_zeit_beginn = timeFormat.format(m_time_beginn);


       // int diff_min = m_time_ende.getHours()*60 + m_time_ende.getHours();
       // m_diff_std = diff_min%60;
        //m_diff_min = diff_min - m_diff_std*60;
       // m_str_zeit_diff= "1"; // Integer.toString(m_diff_std) + ":" + Integer.toString(m_diff_min);

    }

    public void setEnde(Date d)
    {
        m_time_ende = (Date)d.clone();
        m_str_datum = dateFormat.format(m_time_ende);
        m_str_zeit_ende = timeFormat.format(m_time_ende);

        long time_diff_min = (m_time_ende.getTime() - m_time_beginn.getTime())/(1000*60);

        Log.d("time ", Long.toString(time_diff_min));

        m_diff_std =  (int)time_diff_min/60;
        Log.d("time ", Integer.toString(m_diff_std));
        m_diff_min = (int)time_diff_min - m_diff_std*60;
        Log.d("time ", Integer.toString(m_diff_min));

        m_str_zeit_diff= Integer.toString(m_diff_std) + "h, " + Integer.toString(m_diff_min) + "min";
    }


}
