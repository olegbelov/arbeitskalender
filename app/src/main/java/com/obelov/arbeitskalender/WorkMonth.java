package com.obelov.arbeitskalender;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by obelov on 13.11.2016.
 */
public class WorkMonth implements java.io.Serializable{

    public String m_monat;
    public int m_month;

    public double m_hours_to_work;
    public double m_hours;

    public WorkDay[] m_WorkDays;

    public void WorkMonth()
    {
      //  m_WorkDays = new WorkDay[32];
      //  for(int i=0; i<32; i++){
        //    m_WorkDays[i]=null;
       // }
    }

    public void setMonth(int monat)
    {
        m_month = monat;
        m_monat=getMonthString(m_month-1);
        m_WorkDays = new WorkDay[32];
        for(int i=0; i<32; i++){
            m_WorkDays[i]=null;
        }
    }

    public double getHoursToWork()
    {
        return m_hours_to_work;
    }

    public double getHours()
    {
        return m_hours;
    }

    public String getMonthString(int month){
        String[] monthNames = {"Januar", "Februar", "Maerz", "April", "May", "Juni", "Juli",
                "August", "September", "Oktober", "November", "Dezember"};
        return monthNames[month];
    }

    public String getMonthInt(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        return getMonthString(month);
    }
}
