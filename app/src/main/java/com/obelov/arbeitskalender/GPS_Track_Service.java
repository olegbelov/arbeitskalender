package com.obelov.arbeitskalender;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.audiofx.BassBoost;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

/**
 * Created by obelov on 29.11.2016.
 */

@SuppressWarnings("MissingPermission")
public class GPS_Track_Service extends Service {
    public GPS_Track_Service() {
        super();
    }

    private static final String TAG = "BOOMBOOMTESTGPS";
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;
    boolean service_active = false;

    private LocationManager mLocationManager;
    private LocationListener mLocationListener;
    Handler mHandler;


    // La 51.49137835
    // Lo 7.41898233

    // Uni KN
    // 51.491635,
    // 7.411827

    // Arbeit IML
    // 51.49394543
    // 7.40850047

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");


        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Intent i = new Intent("location_update");
                i.putExtra("coordinates", "La:  "+ location.getLatitude()+" Lo:  "+location.getLongitude());

                Location locationKn = new Location("KN");
                locationKn.setLatitude(51.491635);
                locationKn.setLongitude(7.411827);
                double dist = location.distanceTo(locationKn);
                String distS = Double.toString(dist);
                i.putExtra("distance", distS);

                sendBroadcast(i);
                Log.d("Coordinates: ", "Latitude: " + Double.toString(location.getLatitude()) + " Longitude: "+ Double.toString(location.getLongitude()));

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }

            public void requestUpdate(){

                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, 0, mLocationListener);
            }
        };
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        //mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, 0, mLocationListener);

        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                if(message.what==-1)
                {
                    mLocationManager.removeUpdates(mLocationListener);
                    Log.e(TAG, "handleMessageIF");
                }
                else if (message.what == 1){
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, 0, mLocationListener);
                    Log.e(TAG, "handleMessageELSE");
                }
            }
        };


    }






    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        service_active = true;
        TestGPSService();
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }



    void TestGPSService(){
        Log.e(TAG, "TestGPS");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("Message:", "GPS-Tracking");

                int anaus = -1;
                while(service_active == true)
                {
                    try {
                        Message msg = new Message();
                        msg.obj = "on";
                        anaus=anaus*(-1);
                        msg.what = anaus;
                        mHandler.sendMessage(msg);
                        if(anaus==-1)
                            Thread.sleep(60000);
                        else
                            Thread.sleep(10000);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
        ).start();

        /*
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(service_active == true) {
                    Log.d("Message:", "GPS-Tracking");
                    try {
                        TimeUnit.SECONDS.sleep(3);
                        mLocationManager.removeUpdates(mLocationListener);
                        TimeUnit.SECONDS.sleep(3);
                       // mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, mLocationListener);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                stopSelf();
            }
        }).start(); */
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        service_active = false;
        if(mLocationManager != null)
            mLocationManager.removeUpdates(mLocationListener);

        super.onDestroy();
    }


}
